from django.urls import path
from .views import review, reviews


urlpatterns = [
    path("campsites/<int:campsite_id>/reviews/", reviews),
    path("reviews/<int:id>/", review),
]
