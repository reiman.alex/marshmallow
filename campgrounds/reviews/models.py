from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from campsites.models import Campsite

# class Category(models.Model):
#     title = models.CharField(max_length=45)
#     stars = models.SmallIntegerField(
#         validators=[
#             MaxValueValidator(5),
#             MinValueValidator(1)
#         ]
#     )

class Review(models.Model):
    title = models.CharField(max_length=45)
    stars = models.SmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1)
        ]
    )
    date = models.DateField
    stay_again = models.BooleanField()
    # cleanliness = models.ForeignKey(
    #     Category,
    #     related_name="reviews",
    #     on_delete=models.CASCADE
    # )
    # noise = models.ForeignKey(
    #     Category,
    #     related_name="reviews",
    #     on_delete=models.CASCADE
    # )
    # location = models.ForeignKey(
    #     Category,
    #     related_name="reviews",
    #     on_delete=models.CASCADE
    # )
    # quality = models.ForeignKey(
    #     Category,
    #     related_name="reviews",
    #     on_delete=models.CASCADE
    # )
    site = models.ForeignKey(
        Campsite,
        related_name="reviews",
        on_delete=models.CASCADE
    )
    reviewer = models.ForeignKey(
        User,
        related_name="reviews",
        on_delete=models.PROTECT,
    )
