from common.json import ModelEncoder
from .models import Review

class ReviewListEncoder(ModelEncoder):
    model = Review
    properties = [
        "title",
        "stars",
        "date",
        "stay_again",
        "site",
        "reviewer"
    ]

class ReviewDetailEncoder(ModelEncoder):
    model = Review
    properties = [
        "title",
        "stars",
        "date",
        "stay_again",
        "site",
        "reviewer"
    ]
