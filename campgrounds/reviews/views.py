from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Review
from django.http import JsonResponse
from .encoders import ReviewListEncoder, ReviewDetailEncoder
import json
from campsites.models import Campsite

# Create your views here.
@require_http_methods(["GET", "POST"])
def reviews(request, campsite_id):
    # Get's list of reviews
    if request.method == "GET":
        # finds campsite for review
        campsite = Campsite.objects.get(id = campsite_id)
        # filters reviews by campsite
        reviews = Review.objects.filter(site = campsite)
        # gets reviews as JSON
        return JsonResponse(
            {"reviews": reviews},
            encoder = ReviewListEncoder,
            safe = False,
        )
    else:
        content = json.loads(request.body)
        review = Review.objects.create(**content)
        return JsonResponse(
            review,
            encoder=ReviewDetailEncoder,
            safe = False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def review(request, id, campsite_id=None):
    # Get's review info as JSON from a campsite's ID
    if request.method == "GET":
        review = Review.objects.get(id=id)
        return JsonResponse(
            review,
            encoder=ReviewDetailEncoder,
            safe=False,
        )

    # Delete a campsite using its ID
    elif request.method == "DELETE":
        count, _ = Campsite.objects.filter(id=id).delete()

        # Will show "deleted: true" if successful
        return JsonResponse({"deleted": count>0})

    # Update a campsite using its ID
    else:
        content = json.loads(request.body)

        #Check to make sure review is associated to existing campsite
        try:
            campsite = Campsite.objects.get(id=content["site"])
            content["site"] = campsite
        except Campsite.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Campsite"},
                status=400,
            )
        Review.objects.filter(id=id).update(**content)
        review = Review.objects.get(id=id)
        return JsonResponse(
            review,
            encoder=ReviewDetailEncoder,
            safe=False,
        )
