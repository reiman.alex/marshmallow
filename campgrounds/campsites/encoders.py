from common.json import ModelEncoder
from .models import Campsite

class CampsiteListEncoder(ModelEncoder):
    model = Campsite
    properties = [
        "number",
        "campground",
    ]

class CampsiteDetailEncoder(ModelEncoder):
    model = Campsite
    properties = [
        "number",
        "site_type",
        "fire_pit",
        "sewer_hookup",
        "water_hookup",
        "pets_allowed",
        "waterfront",
        "shade",
        "campground",
    ]
