from django.db import models
from campgrounds.models import Campground

# Create your models here.
class Campsite(models.Model):
    number = models.PositiveSmallIntegerField()

    tent = "Tent"
    standard_non_electric = "Standard Non-Electric"
    rv = "RV"
    group = "Group"
    type_choices = [
        (tent, "Tent"),
        (standard_non_electric, "Standard Non-Electric"),
        (rv, "RV"),
        (group, "Group"),
    ]
    site_type = models.CharField(
        max_length=21,
        choices=type_choices,
        default=standard_non_electric,
    )
    fire_pit = models.BooleanField(default=True)
    sewer_hookup = models.BooleanField(default=False)
    water_hookup = models.BooleanField(default=False)
    pets_allowed = models.BooleanField(default=False)
    waterfront = models.BooleanField(default=False)
    shade = models.BooleanField(default=False)

    campground = models.ForeignKey(
        Campground,
        related_name="campsites",
        on_delete=models.CASCADE,
        default=None
    )

    def __str__(self):
        return self.number
