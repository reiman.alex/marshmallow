from django.urls import path
from .views import campsites, campsite


urlpatterns = [
    path("campsites/", campsites),
    path("campsites/<int:id>/", campsite),
]
