from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Campsite
from django.http import JsonResponse
from .encoders import CampsiteListEncoder, CampsiteDetailEncoder
import json
from campgrounds.models import Campground

@require_http_methods(["GET", "POST"])
def campsites(request):
    if request.method == "GET":
        campsites = Campsite.objects.all()
        return JsonResponse(
            {"campsites": campsites},
            encoder = CampsiteListEncoder,
            safe = False,
        )
    else:
        content = json.loads(request.body)
        campsite = Campsite.objects.create(**content)
        return JsonResponse(
            campsite,
            encoder=CampsiteDetailEncoder,
            safe = False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def campsite(request, id):

    # Get's campsite info as JSON from a campsite's ID
    if request.method == "GET":
        campsite = Campsite.objects.get(id=id)
        return JsonResponse(
            campsite,
            encoder=CampsiteDetailEncoder,
            safe=False,
        )

    # Delete a campsite using its ID
    elif request.method == "DELETE":
        count, _ = Campsite.objects.filter(id=id).delete()

        # Will show "deleted: true" if successful
        return JsonResponse({"deleted": count>0})

    # Update a campsite using its ID
    else:
        content = json.loads(request.body)

        #Check to make sure campsite is associated to existing campground
        try:
            campground = Campground.objects.get(id=content["id"])
            content["campground"] = campground
        except Campground.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Campground"},
                status=400,
            )
        Campsite.objects.filter(id=id).update(**content)
        campsite = Campsite.objects.get(id=id)
        return JsonResponse(
            campsite,
            encoder=CampsiteDetailEncoder,
            safe=False,
        )
