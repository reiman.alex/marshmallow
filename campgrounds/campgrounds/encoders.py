from common.json import ModelEncoder
from .models import Campground

class CampgroundListEncoder(ModelEncoder):
    model = Campground
    properties = [
        "id",
        "name",
    ]

class CampgroundDetailEncoder(ModelEncoder):
    model = Campground
    properties = [
        "id",
        "name",
        "address",
        "phone",
        "website",
        "email",
        "tent_sites",
        "rv_sites",
        "reservable",
        "trash",
        "firewood_available",
        "wifi",
        "drinking_water",
        "showers",
        "bathroom",
    ]
