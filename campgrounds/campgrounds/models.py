from django.db import models
# from phonenumber_field.modelfields import PhoneNumberField

class Campground(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length = 10, default=False)
    website = models.URLField(null=True)
    email = models.EmailField()
    tent_sites = models.PositiveSmallIntegerField()
    rv_sites = models.PositiveSmallIntegerField()
    reservable = models.BooleanField(default=False)
    trash = models.BooleanField(default=False)
    firewood_available = models.BooleanField(default=False)
    wifi = models.BooleanField(default=False)
    drinking_water = models.BooleanField(default=False)
    showers = models.BooleanField(default=False)

    none = "None"
    vault = "Vault"
    flush = "Flush"
    bathroom_choices = [
        (none, "None"),
        (vault, "Vault"),
        (flush, "Flush"),
    ]
    bathroom = models.CharField(
        max_length=5,
        choices=bathroom_choices,
        default = none
    )
