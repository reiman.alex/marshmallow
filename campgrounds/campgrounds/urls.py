from django.urls import path
from .views import campgrounds, campground


urlpatterns = [
    path("campgrounds/", campgrounds),
    path("campgrounds/<int:id>/", campground),
]
