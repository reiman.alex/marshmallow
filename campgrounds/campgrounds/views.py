from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Campground
from django.http import JsonResponse
from .encoders import CampgroundListEncoder, CampgroundDetailEncoder
import json

@require_http_methods(["GET", "POST"])
def campgrounds(request):
    if request.method == "GET":
        campgrounds = Campground.objects.all()
        return JsonResponse(
            {"campgrounds": campgrounds},
            encoder = CampgroundListEncoder,
            safe = False,
        )
    else:
        content = json.loads(request.body)
        campground = Campground.objects.create(**content)
        return JsonResponse(
            campground,
            encoder=CampgroundDetailEncoder,
            safe = False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def campground(request, id):
    if request.method == "GET":
        campground = Campground.objects.get(id=id)
        return JsonResponse(
            campground,
            encoder=CampgroundDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Campground.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        Campground.objects.filter(id=id).update(**content)
        campground = Campground.objects.get(id=id)
        return JsonResponse(
            campground,
            encoder=CampgroundDetailEncoder,
            safe=False,
        )
